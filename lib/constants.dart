import 'package:flutter/material.dart';

class NavKeys {
  static final GlobalKey<NavigatorState> main = GlobalKey();
}

const mainColor = Color(0XFF0D6EFD);
const mainsSubColor = Color(0x995B9EE1);
const greyColor = Color(0xFF2B2B2B);
const whiteColor = Color(0xFFFFFFFF);
const blueColor = Color(0xFF1583c2);
const yellowColor = Color(0xFFffb21c);
const backgroundColor = Color(0xFFF7F7F9);
const redColor = Color(0xFFFD1600);
const shadowColor = Color(0x0A000000);
const textColor = Color(0xFF6A6A6A);
const subTextColor = Color(0xFF3B3B3B);
const highlightTextColor = Color(0xFF674DC5);
const greyLowColor = Color(0xFFebebec);
const skyColor = Color(0xFFdff0ff);

const backgroundNav = "assets/images/background_navigat.png";
const homeSvg = "assets/svgs/home.svg";
const favoriteSvg = "assets/svgs/heart.svg";
const cartSvg = "assets/svgs/cart.svg";
const profileSvg = "assets/svgs/profile.svg";
const notificationSvg = "assets/svgs/notification.svg";
const nikeSvg = "assets/svgs/Vector.svg";
const shoseStyle = "assets/images/shose_style.png";
const logo_nike = "assets/images/nike_logo.png";
const shose2 = "assets/images/shose2.png";
const shose3 = "assets/images/shose3.png";
const drawAbleSvg = "assets/svgs/menu_btn.svg";
const searchSvg = "assets/svgs/search.svg";
const filterSvg = "assets/svgs/filter.svg";
const favoriteFullSvg = "assets/svgs/heart_full.svg";
const backSvg = "assets/svgs/Arrow.svg";
const addSvg = "assets/svgs/add.svg";
const cardSvg = "assets/svgs/card.svg";
const cashSvg = "assets/svgs/cash.svg";
const emailSvg = "assets/svgs/email.svg";
const phoneSvg = "assets/svgs/phone.svg";
const editSvg = "assets/svgs/edit.svg";
const mapSvg = "assets/svgs/location.svg";
const successPng = "assets/images/success.png";
const googleSvg = "assets/svgs/google.svg";
const increaseSvg = "assets/svgs/increase.svg";
const addBoldSvg = "assets/svgs/add-svgrepo-com.svg";
