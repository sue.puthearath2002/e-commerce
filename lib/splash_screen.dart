import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/screens/onboard/onboard_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});
  static const String routeName = "/splash_screen";

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(
        const Duration(milliseconds: 600),
        () => Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => const OnBoardScreen())));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              nikeSvg,
              width: 157,
              height: 54,
              colorFilter: const ColorFilter.mode(whiteColor, BlendMode.srcIn),
            ),
            const SizedBox(height: 10),
            const Text(
              "NIKE",
              style: TextStyle(
                fontFamily: 'Raleway',
                fontSize: 65,
                fontWeight: FontWeight.w700,
                color: whiteColor,
              ),
            )
          ],
        ),
      ),
    );
  }
}
