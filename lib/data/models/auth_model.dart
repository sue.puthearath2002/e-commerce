class AuthModel {
  final String token;
  final String username;
  final String firstName;
  final String lastName;
  final String email;
  final String phoneNo;
  final String address;
  final String avatar128;
  final String avatar;

  AuthModel({
    required this.token,
    required this.username,
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.phoneNo,
    required this.address,
    required this.avatar128,
    required this.avatar,
  });

  factory AuthModel.fromJson(Map<String, dynamic> json) => AuthModel(
        token: json["token"] ?? "",
        username: json["username"] ?? "",
        firstName: json["first_name"] ?? "",
        lastName: json["last_name"] ?? "",
        email: json["email"] ?? "",
        phoneNo: json["phone_no"] ?? "",
        address: json["address"] ?? "",
        avatar128: json["avatar_128"] ?? "",
        avatar: json["avatar"] ?? "",
      );

  Map<String, dynamic> toJson() => {
        "token": token,
        "username": username,
        "first_name": firstName,
        "last_name": lastName,
        "email": email,
        "phone_no": phoneNo,
        "address": address,
        "avatar_128": avatar128,
        "avatar": avatar,
      };
}
