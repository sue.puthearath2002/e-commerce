
import 'package:shose_shop/data/models/model.dart';

class UserModel extends Model {
  static const String tableName = "users";
  final String token;
  final String username;
  final String firstName;
  final String lastName;
  final String email;
  final String phoneNo;
  final String address;
  final String avatar128;
  final String avatar;

  const UserModel({
    required this.token,
    required this.username,
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.phoneNo,
    required this.address,
    required this.avatar128,
    required this.avatar,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        token: json["token"] ?? "",
        username: json["username"] ?? "",
        firstName: json["first_name"] ?? "",
        lastName: json["last_name"] ?? "",
        email: json["email"] ?? "",
        phoneNo: json["phone_no"] ?? "",
        address: json["address"] ?? "",
        avatar128: json["avatar_128"] ?? "",
        avatar: json["avatar"] ?? "",
      );

  @override
  List<Object?> get props => [];
}
