class UserDetailModel {
  final String email;
  final String userName;
  final String phone;
  final String userEmail;
  final String imgUrl;
  final String locale;

  UserDetailModel({
    required this.email,
    required this.userName,
    required this.phone,
    required this.userEmail,
    required this.imgUrl,
    required this.locale,
  });

  factory UserDetailModel.fromJson(Map<String, dynamic> json) =>
      UserDetailModel(
        email: json["email"] ?? "",
        userName: json["userName"] ?? "",
        phone: json["phone"] ?? "",
        userEmail: json["userEmail"] ?? "",
        imgUrl: json["imgUrl"] ?? "",
        locale: json["locale"] ?? "",
      );

  Map<String, dynamic> toJson() => {
        "email": email,
        "userName": userName,
        "phone": phone,
        "userEmail": userEmail,
        "imgUrl": imgUrl,
        "locale": locale,
      };
}
