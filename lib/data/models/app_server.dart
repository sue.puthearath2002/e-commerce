
import 'package:shose_shop/data/models/model.dart';

class AppServer extends Model {
  const AppServer({required this.url});

  static const String tableName = "app_server";
  final String url;

  @override
  List<Object?> get props => [];
}
