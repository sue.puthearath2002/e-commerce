class ApiResponse<T> {
  String status;
  final String msg;
  T records;
  final Map? response;
  final int lastPage;
  final int currentPage;
  final Object? errors;

  ApiResponse({
    required this.status,
    this.msg = "",
    required this.records,
    this.lastPage = 1,
    this.currentPage = 1,
    this.errors,
    this.response,
  });
}
