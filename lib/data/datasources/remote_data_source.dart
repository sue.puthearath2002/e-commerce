import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'remote_data.dart';

class ImpRemoteDataSource implements RemoteDataSource {
  final http.Client client;

  ImpRemoteDataSource({required this.client});

  String baseUrl = "https://192.168.40.22";
  //String domain = "https://192.168.40.22/api/app-delivery/v1";
  String domain = "http://192.168.40.23/api/app-delivery/v1";

  // Future<String> getParams(Map? params) async {
  //   try {
  //     final auth = await Auth.instance.user;

  //     final Map param = {
  //       "app_id": "com.clearviewerp.salesforce",
  //       "token": auth?.token,
  //       "source": Platform.isIOS ? "ios" : "android",
  //     };

  //     var body = param;
  //     if (params != null) {
  //       body = {...param, ...params};
  //     }

  //     return json.encode(body);
  //   } catch (e) {
  //     return "";
  //   }
  // }

  // @override
  // Future<ApiResponse> login({Map? params}) async {
  //   var url = Uri.parse("$domain/login");

  //   //010445566
  //   //

  //   final result = await client.post(
  //     url,
  //     headers: {"Content-Type": "application/json"},
  //     body: await getParams(params),
  //   );
  //   print(await getParams(params));
  //   if (result.statusCode != 200) {
  //     throw ServerException();
  //   }

  //   final getResponse = json.decode(result.body);
  //   print(getResponse);
  //   return ApiResponse(
  //     status: getResponse["status"] ?? "success",
  //     records: getResponse['records'],
  //     response: getResponse,
  //     msg: getResponse["message"] ?? "",
  //   );
  // }

  // @override
  // Future<ApiResponse> getUserProfile({Map? params}) async {
  //   var url = Uri.parse("$domain/get-user-profile");
  //   final result = await client.post(
  //     url,
  //     headers: {"Content-Type": "application/json"},
  //     body: await getParams(params),
  //   );
  //   print(await getParams(params));
  //   if (result.statusCode != 200) {
  //     throw ServerException();
  //   }

  //   final getResponse = json.decode(result.body);
  //   print(getResponse);
  //   return ApiResponse(
  //     status: getResponse["status"] ?? "success",
  //     records: getResponse['user'],
  //     msg: getResponse["msg"] ?? "Success",
  //   );
  // }

}
