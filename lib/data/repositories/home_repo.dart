
import 'package:shose_shop/data/datasources/remote_data.dart';

class HomeRepo {
  final RemoteDataSource source;

  const HomeRepo(this.source);
}
