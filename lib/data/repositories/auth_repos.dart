import 'dart:io';


import 'package:shose_shop/data/datasources/remote_data.dart';
import 'package:shose_shop/data/repositories/repo_response.dart';
import 'package:sqflite/sqflite.dart' as sql;

class AuthRepos {
  AuthRepos(this.source);

  final RemoteDataSource source;

  // Future<Either<Failure, RepoResponse<Map>>> login(Map params) async {
  //   try {
  //     final result = await source.login(params: params);
  //     if (result.status != "success") {
  //       return Left(ServerFailure(result.msg));
  //     }
  //     var record = result.records as Map;
  //     await _updateAuth(record);
  //     Auth.instance.setAuth();

  //     return Right(RepoResponse(
  //       msg: result.status,
  //       records: record,
  //     ));
  //   } on ServerException {
  //     return Left(ServerFailure(errorMessage));
  //   } on SocketException {
  //     return Left(NetworkFailure(errorInternetMessage));
  //   }
  // }

  // Future<void> _updateAuth(Object record) async {
  //   var auth = record as Map;
  //   final db = await DB.instance.db;

  //   await db.delete(UserModel.tableName);

  //   final data = {
  //     //"id": auth['id'] ?? "",
  //     "email": auth['email'] ?? "",
  //     "token": auth['token'] ?? "",
  //     "username": auth['username'] ?? "",
  //     "address": auth['address'] ?? "",
  //     "phone_no": auth['phone_no'] ?? "",
  //     'first_name': auth['first_name'] ?? "",
  //     'last_name': auth['last_name'] ?? "",
  //     'avatar_128': auth['avatar_128'] ?? "",
  //     'avatar': auth['avatar'] ?? "",
  //   };

  //   await db.insert(
  //     UserModel.tableName,
  //     data,
  //     conflictAlgorithm: sql.ConflictAlgorithm.replace,
  //   );
  // }

  // Future<int> _checkAuth() async {
  //   final db = await DB.instance.db;
  //   var x = await db.rawQuery('SELECT COUNT (*) FROM ${UserModel.tableName}');
  //   return sql.Sqflite.firstIntValue(x) ?? 0;
  // }

  // Future<void> updateConfig(String url) async {
  //   final db = await DB.instance.db;

  //   await db.insert(
  //     AppServer.tableName,
  //     {"domain_name": url, "type": "share"},
  //     conflictAlgorithm: sql.ConflictAlgorithm.replace,
  //   );
  // }
}
