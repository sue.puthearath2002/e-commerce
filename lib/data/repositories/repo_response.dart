class RepoResponse<T> {
  T records;
  final int? lastPage;
  final int? currentPage;
  final String? msg;

  RepoResponse({
    this.lastPage = 1,
    this.currentPage = 1,
    required this.records,
    this.msg = "",
  });
}
