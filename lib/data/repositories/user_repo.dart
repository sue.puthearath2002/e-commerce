import 'dart:io';

import 'package:shose_shop/data/datasources/remote_data.dart';
import 'package:shose_shop/data/models/user_detail_model.dart';
import 'package:shose_shop/data/repositories/repo_response.dart';


class UserRepo {
  UserRepo(this.source);
  final RemoteDataSource source;
  // Future<Either<Failure, RepoResponse<UserDetailModel>>> userProfile(
  //     Map params) async {
  //   try {
  //     final result = await source.getUserProfile(params: params);
  //     if (result.status != "success") {
  //       return Left(ServerFailure(result.msg));
  //     }
  //     final record = UserDetailModel.fromJson(result.records);
  //     return Right(RepoResponse(
  //       records: record,
  //     ));
  //   } on ServerException {
  //     return Left(ServerFailure(errorMessage));
  //   } on SocketException {
  //     return Left(NetworkFailure(errorInternetMessage));
  //   }
  // }
}
