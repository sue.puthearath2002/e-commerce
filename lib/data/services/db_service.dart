import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DB {
  DB._();

  static final DB instance = DB._();

  static Database? _database;

  Future<Database> get db async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String p = "";
    if (Platform.isIOS) {
      p = "/Users/annyon/Documents";
    } else {
      p = directory.path;
    }

    String path = "$p/delivery.db"; //join(p, "delivery.db");

    return await openDatabase(
      path,
      version: 1,
      onCreate: (db, version) => _createTables(db),
      onUpgrade: (db, oldVersion, newVersion) async {
        var batch = db.batch();
        if (oldVersion == 1) {
          _updateTableCompanyV1toV2(batch);
        }
        await batch.commit();
      },
    );
  }

  Future _createTables(Database db) async {
    var batch = db.batch();

    batch.execute('''CREATE TABLE users (
      id INTEGER PRIMARY KEY, 
      email TEXT, 
      user_type TEXT,
      username TEXT,
      first_name TEXT,
      last_name TEXT,
      phone_no TEXT,
      address TEXT,
      avatar TEXT,
      avatar_128 TEXT,
      organization_name TEXT,
      database_name TEXT,
      account_id INTEGER,
      locale TEXT,
      token TEXT,
      time_zone TEXT,
      login_date REAL
    )''');

    batch.execute('''CREATE TABLE user_setups (
      id INTEGER PRIMARY KEY, 
      user_id INTEGER,
      email TEXT, 
      username TEXT,
      user_type TEXT,
      first_name TEXT,
      last_name TEXT,
      phone_no TEXT,
      avatar TEXT,
      organization_name TEXT,
      database_name TEXT,
      location_code TEXT,
      intransit_location_code TEXT,
      business_unit_code TEXT,
      store_code TEXT,
      division_code TEXT,
      project_code TEXT,
      salesperson_code TEXT,
      department_code TEXT,
      distributor_code TEXT,
      hide_item_cost TEXT,
      permission_code TEXT,
      role_code TEXT,
      customer_no TEXT,
      vendor_no TEXT,
      ash_journal_batch_name TEXT,
      cash_bank_account_code TEXT,
      pay_journal_batch_name TEXT,
      gen_journal_batch_name TEXT,
      item_journal_batch_name TEXT,
      phy_inv_journal_batch_name TEXT
    )''');

    batch.execute('''CREATE TABLE settings (
      id INTEGER PRIMARY KEY, 
      crm_license TEXT, 
      gps_license TEXT,
      max_upload_file_size INTEGER,
      max_upload_file_size_human TEXT,
      last_settlement_date REAL
    )''');

    batch.execute('''CREATE TABLE application_setup (
      id INTEGER PRIMARY KEY, 
      connect_to TEXT
      decimalpoint TEXT,
      separator_symbol TEXT,
      quantity_decimal INTEGER,
      price_decimal INTEGER,
      cost_decimal INTEGER,
      measurement_decimal INTEGER,
      general_decimal INTEGER,
      amount_decimal INTEGER,
      percentage_decimal INTEGER,
      item_qty_format INTEGER DEFAULT 5,
      allow_posting_from TEXT,
      allow_posting_to TEXT,
      local_currency_code TEXT DEFAULT "USD",
      decimal_zero TEXT DEFAULT "Yes",
      income_closing_period TEXT DEFAULT "Yearly",
      scroll_pagination TEXT,
      default_sales_vat_acc_no TEXT,
      default_purchase_vat_acc_no TEXT,
      default_ap_acc_no TEXT,
      default_ar_acc_no TEXT,
      default_bank_acc_no TEXT,
      default_cash_acc_no TEXT,
      default_cost_acc_no TEXT,
      default_sales_acc_no TEXT,
      default_purchase_acc_no TEXT,
      default_inventory_acc_no TEXT,
      default_positive_adj_account_no TEXT,
      default_negative_adj_account_no TEXT,
      default_inv_posting_group TEXT,
      default_ap_posting_group TEXT,
      default_ar_posting_group TEXT,
      default_gen_bus_posting_group TEXT,
      default_gen_prod_posting_group TEXT,
      default_vat_bus_posting_group TEXT,
      default_vat_prod_posting_group TEXT,
      default_payment_term TEXT,
      default_stock_unit_measure TEXT,
      default_item_price_include_vat TEXT,
      accept_eorder_order_status TEXT,
      auto_accept_incoming_eorder TEXT
    )''');

    batch.execute('''CREATE TABLE app_server (
      id INTEGER PRIMARY KEY, 
      description TEXT, 
      domain_name INTEGER,
      type TEXT
    )''');

    batch.execute('''CREATE TABLE company (
      id INTEGER PRIMARY KEY, 
      name TEXT,
      name_2 TEXT,
      house_no TEXT,
      street_no TEXT,
      address TEXT,
      address_2 TEXT,
      post_code TEXT,
      village TEXT,
      commune TEXT,
      district TEXT,
      province TEXT,
      country_code TEXT,
      vat_registration_no TEXT,
      phone_no TEXT,
      fax_no TEXT,
      email TEXT,
      website TEXT,
      logo TEXT,
      logo32 TEXT,
      logo128 TEXT,
      business_industry TEXT,
      sub_business_industry TEXT,
      document_template TEXT,
      header TEXT,
      header_picture TEXT,
      footer TEXT,
      footer_picture TEXT,
      account_no TEXT,
      monday TEXT,
      tuesday TEXT,
      wednesday TEXT,
      thursday TEXT,
      friday TEXT,
      saturday TEXT,
      sunday TEXT,
      start_time REAL,
      end_time REAL
    )''');

    await batch.commit();
  }

  void _updateTableCompanyV1toV2(Batch batch) {
    // batch.execute('ALTER TABLE cart ADD tesing TEXT');
  }
}
