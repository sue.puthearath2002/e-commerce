import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/widget/app_bar.dart';
import 'package:shose_shop/widget/divider_line.dart';
import 'package:shose_shop/widget/header_content.dart';
import 'package:shose_shop/widget/main_btn.dart';
import 'package:shose_shop/widget/main_shape_container.dart';
import 'package:shose_shop/widget/radio_shape.dart';
import 'package:shose_shop/widget/text_help.dart';

class DetailCheckOut extends StatefulWidget {
  const DetailCheckOut({super.key});
  static const String routeName = "/detail-checkout";

  @override
  State<DetailCheckOut> createState() => _DetailCheckOutState();
}

class _DetailCheckOutState extends State<DetailCheckOut> {
  List<Map<String, String>> paymentMethod = [
    {"icon": cardSvg, "title": "Online Payment"},
    {"icon": cashSvg, "title": "Cash On Delivery"},
  ];
  int isSelected = 0;
  void showAlertDialog(BuildContext context) {
    showDialog(
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: MainShapeContainer(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    width: 140,
                    height: 140,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: skyColor,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SizedBox(
                          width: 150,
                          height: 150,
                          child: Image.asset(successPng)),
                    ),
                  ),
                  const SizedBox(height: 20),
                  const TextHelp(text: "Your Payment is Success"),
                  const SizedBox(height: 20),
                  MainButton(
                    lable: "Back To Shopping",
                    onTap: () {
                      Navigator.pop(context);
                    },
                    color: mainColor,
                    colorText: Colors.white,
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData().copyWith(
        dividerColor: Colors.transparent,
      ),
      child: Scaffold(
        appBar: const AppbarCus(title: "Info Order", isDrawAble: false),
        persistentFooterButtons: [
          MainButton(
            color: mainColor,
            colorText: whiteColor,
            paddingBtn: 0,
            lable: "Check Out",
            onTap: () {
              setState(() {
                showAlertDialog(context);
              });
            },
          ),
        ],
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              MainShapeContainer(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const HeaderContent(
                        leftText: "Total", rightText: "\$45.90"),
                    const SizedBox(height: 10),
                    const DividerLine(
                      color: greyColor,
                    ),
                    const SizedBox(height: 10),
                    const TextHelp(text: "Pay with"),
                    const SizedBox(height: 10),
                    Column(
                      children: List.generate(
                        paymentMethod.length,
                        (index) => InkWell(
                          onTap: () {
                            setState(() {
                              isSelected = index;
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: checkPayment(
                              iconSvg: paymentMethod[index]["icon"],
                              title: paymentMethod[index]["title"],
                              isSelect: isSelected == index ? true : false,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 10),
              MainShapeContainer(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const TextHelp(text: "Contact Information"),
                  const SizedBox(height: 16),
                  contactInfo(
                      iconSvg: emailSvg,
                      title: "Info232@gmail.com",
                      onTap: () {}),
                  const SizedBox(height: 10),
                  contactInfo(
                      iconSvg: phoneSvg, title: "098766689", onTap: () {})
                ],
              )),
              const SizedBox(height: 10),
              MainShapeContainer(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const TextHelp(text: "Address"),
                  const SizedBox(height: 16),
                  contactInfo(
                      iconSvg: mapSvg,
                      title: "Phnom Penh, Phnom Penh, Combodia",
                      onTap: () {}),
                  const SizedBox(height: 10),
                  Container(
                    color: blueColor,
                    width: double.infinity,
                    height: 100,
                  )
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }

  Widget contactInfo({String? iconSvg, String? title, VoidCallback? onTap}) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Row(
        children: [
          MainShapeContainer(
            padding: 8,
            colorContainer: greyLowColor,
            child: SvgPicture.asset(
              iconSvg ?? "",
              width: 24,
              height: 24,
            ),
          ),
          const SizedBox(width: 20),
          TextHelp(
            text: title ?? "",
            isOverFlow: true,
            fontFamily: "Poppins",
          ),
        ],
      ),
      InkWell(onTap: onTap, child: SvgPicture.asset(editSvg))
    ]);
  }

  Widget checkPayment({String? iconSvg, String? title, bool? isSelect = true}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            MainShapeContainer(
              padding: 8,
              colorContainer: greyLowColor,
              child: SvgPicture.asset(
                iconSvg ?? "",
                width: 24,
                height: 24,
              ),
            ),
            const SizedBox(width: 20),
            TextHelp(
              text: title ?? "",
              fontFamily: "Poppins",
            ),
          ],
        ),
        RadioShape(
          isSelect: isSelect,
        ),
      ],
    );
  }
}
