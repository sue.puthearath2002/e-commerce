import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/screens/carts/cart_screen.dart';
import 'package:shose_shop/screens/cubits/home_cubit/home_cubit.dart';
import 'package:shose_shop/screens/home/home_screen.dart';
import 'package:shose_shop/screens/notification/notification.dart';
import 'package:shose_shop/screens/profile/profile_screen.dart';
import 'package:badges/badges.dart' as badges;
import 'package:shose_shop/widget/circle_btn.dart';
import 'favorites/favorite_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  List<String> listOfIcons = [
    homeSvg,
    favoriteSvg,
    "",
    notificationSvg,
    profileSvg
  ];
  List<Widget> listOfWidget = [
    HomeScreen(),
    const FavoriteScreen(),
    const CartScreen(),
    const NotificationScreen(),
    const ProfileScreen()
  ];
  PageController pageController =
      PageController(viewportFraction: 1, keepPage: true);
  int selectIndex = 0;
  final screenCubit = HomeCubit();

  @override
  void initState() {
    screenCubit.onInitCart();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pageViewCustome(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            //selectIndex = 2;
            //pageController.jumpToPage(2);
          });
          Navigator.pushNamed(context, CartScreen.routeName);
        },
        child: BlocConsumer<HomeCubit, HomeState>(
          bloc: screenCubit,
            listener: (context, state) {},
            builder: (context, state) {
              final data = state.cartNumber;
              print("ooooasdfao${data}");
              return badges.Badge(
                badgeAnimation: const badges.BadgeAnimation.rotation(
                  animationDuration: Duration(seconds: 1),
                  colorChangeAnimationDuration: Duration(seconds: 1),
                  loopAnimation: false,
                  curve: Curves.fastOutSlowIn,
                  colorChangeAnimationCurve: Curves.easeInCubic,
                ),
                badgeContent: Text(
                  data.toString(),
                  style: const TextStyle(color: whiteColor, fontSize: 18),
                ),
                child: const CirCleBtn(
                  iconSvg: cartSvg,
                  isShadow: true,
                ),
              );
            },
          ),
        
      ),
      bottomNavigationBar: SizedBox(
        height: 70,
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            SizedBox(
                height: 65,
                width: MediaQuery.of(context).size.width,
                child: Image.asset(
                  backgroundNav,
                  fit: BoxFit.cover,
                )),
            Positioned(
              bottom: 20,
              left: 0,
              right: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: List.generate(
                  listOfIcons.length,
                  (index) {
                    return Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: IconButton(
                        onPressed: () {
                          setState(() {
                            selectIndex = index;
                            pageController.jumpToPage(index);
                            //print(selectIndex);
                            print(index);
                          });
                        },
                        icon: SvgPicture.asset(
                          listOfIcons[index],
                          colorFilter: selectIndex == index
                              ? const ColorFilter.mode(
                                  mainColor, BlendMode.srcIn)
                              : const ColorFilter.mode(
                                  greyColor, BlendMode.srcIn),
                          width: 22,
                          height: 24,
                        ),
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget pageViewCustome() {
    return PageView(
      physics: const NeverScrollableScrollPhysics(),
      controller: pageController,
      children:
          List.generate(listOfWidget.length, (index) => listOfWidget[index]),
    );
  }
}
