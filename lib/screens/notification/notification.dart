import 'package:flutter/material.dart';
import 'package:shose_shop/widget/app_bar.dart';

class NotificationScreen extends StatelessWidget {
  const NotificationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: AppbarCus(
        isDrawAble: true,
        title: "Notification",
      ),
    );
  }
}
