import 'package:flutter/material.dart';
import 'package:shose_shop/widget/app_bar.dart';
import 'package:shose_shop/widget/card_product.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({super.key});

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppbarCus(
        isDrawAble: true,
        title: "Favorite",
        showDrawer: () {
          scaffoldKey.currentState!.openDrawer();
        },
      ),
      body: buildBody(),
    );
  }

  Widget buildBody() {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 0.75,
      ),
      itemCount: 10, // total number of items
      itemBuilder: (context, index) {
        return const Padding(
          padding: EdgeInsets.all(16),
          child: CardProduct(
              IsFullFavorite: true,
              image:
                  "https://www.golfdigest.com/content/dam/images/golfdigest/products/2022/3/3/20220302_NikeAirJordanLows1.jpg",
              status: "BEST SELLER",
              nameProduct: "Nike Jordon",
              priceProduct: "\$302.90"),
        );
      },
    );
  }
}
