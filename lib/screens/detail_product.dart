import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/widget/app_bar.dart';
import 'package:shose_shop/widget/box_shadow_container.dart';
import 'package:shose_shop/widget/cache_image.dart';
import 'package:shose_shop/widget/card_product.dart';
import 'package:shose_shop/widget/circle_btn.dart';
import 'package:shose_shop/widget/header_content.dart';
import 'package:shose_shop/widget/main_btn.dart';
import 'package:shose_shop/widget/text_help.dart';

class DetailScreen extends StatefulWidget {
  const DetailScreen({super.key});
  static const String routeName = "/detail-screen";

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  String selectVariant = "";
  final kGradientBoxDecoration = const BoxDecoration(
    gradient: LinearGradient(colors: [Colors.black, Colors.redAccent]),
  );
  List<String> listVariant = [
    "10%",
    "20%",
    "30%",
    "40%",
    "50%",
    "60%",
    "70%",
    "80%",
    "90%",
    "100%",
  ];
  List<Map<String, String>> listShoes = [
    {
      "color": "green",
      "image":
          'https://rukminim2.flixcart.com/image/850/1000/xif0q/shoe/t/3/d/9-creta-12-pro-asian-light-grey-original-imaggm7fwu9wczyh.jpeg?q=90'
    },
    {
      "color": "blue",
      "image": 'https://i8.amplience.net/i/jpl/jd_556031_a?qlt=92'
    },
    {
      "color": "yellow",
      "image":
          'https://www.asphaltgold.com/cdn/shop/products/f6725e8acae3d99bc1e071eda1f5422b4089175d_DO1925_003_Nike_Jumpman_Two_Trey_Black_University_Red_Black_White_os_1_768x768.jpg?v=1692363944'
    },
    {
      "color": "red",
      "image":
          'https://szopex.blob.core.windows.net/shops/media/f1000/2023/nike/215824/buty-nike-jumpman-two-trey-anthracite-black-cement-grey-white-dr9631-003-63e23d88c98b6.jpg'
    },
    {
      "color": "sky",
      "image": 'https://m.media-amazon.com/images/I/61zL7NqwAeS._AC_UY1000_.jpg'
    },
  ];
  int selected = 0;
  String color = "";
  PageController _controller = PageController();
  // _listener() {
  //   setState(() {
  //     if (_controller.position.userScrollDirection == ScrollDirection.reverse) {
  //     } else {
  //       _controller.position.userScrollDirection == ScrollDirection.forward;
  //     }
  //   });
  // }

  @override
  void initState() {
    //_controller = PageController()..addListener(_listener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppbarCus(title: "Cappuccino"),
      body: buildBody(context),
    );
  }

  Widget buildBody(BuildContext context) {
    return Stack(
      children: [
        CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    showProduct(),
                    titleProduct(),
                    const SizedBox(height: 8),
                    const TextHelp(
                      text: "Hot Coffees",
                      fontWeight: FontWeight.w500,
                      fonSize: 16,
                      color: greyColor,
                    ),
                    const SizedBox(height: 8),
                    const SizedBox(
                      width: 300,
                      child: TextHelp(
                        text: "\$4",
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w700,
                        fonSize: 20,
                        color: textColor,
                      ),
                    ),
                    // const SizedBox(height: 120),
                    // showPictureProduct(context),
                    // const SizedBox(height: 16),
                    // attributeLsist(),
                    const SizedBox(height: 16),
                    descriptionsProduct(),
                    const SizedBox(height: 16),
                    const TextHelp(
                      text: " Sugar",
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w700,
                      fonSize: 16,
                      color: textColor,
                    ),
                    listOfVariant(),
                    const SizedBox(height: 16),
                    const HeaderContent(
                      leftText: "Recommend",
                      rightText: "See all",
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                  ],
                ),
              ),
            ),
            SliverGrid(
              delegate: SliverChildBuilderDelegate(
                (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(16),
                    child: CardProduct(
                        onTap: () {
                          Navigator.pushNamed(context, DetailScreen.routeName);
                        },
                        image:
                            "https://upload.wikimedia.org/wikipedia/commons/thumb/5/58/Mocha_Latte_Costa_Rica.JPG/1920px-Mocha_Latte_Costa_Rica.JPG",
                        status: "BEST SELLER",
                        nameProduct: "Cappuccino",
                        priceProduct: "\$3.90"),
                  );
                },
                childCount: 5,
              ),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 0.75,
              ),
            ),
          ],
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Container(
            //height: 90,
            color: whiteColor,
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CirCleBtn(
                    onTap: () {},
                    width: 55,
                    height: 55,
                    paddingIconSvg: 10,
                    iconSvg: favoriteSvg,
                    colorIconSvg: greyColor,
                    colorContainer: greyLowColor,
                  ),
                  MainButton(
                    isIconsImage: true,
                    lable: "Add To Cart",
                    onTap: () {},
                    width: MediaQuery.of(context).size.width * 0.7,
                    color: mainColor,
                    colorText: whiteColor,
                    paddingBtn: 8,
                    //paddingBtn: 8,
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget showProduct() {
    return SizedBox(
        width: double.infinity,
        height: 300,
        child: Image.asset("assets/images/kapotino.png"));
  }

  Widget listOfVariant() {
    return Wrap(
      children: List.generate(
          listVariant.length,
          (index) => MainButton(
                width: 60,
                color:
                    selectVariant == listVariant[index] ? blueColor : skyColor,
                lable: listVariant[index],
                colorText: selectVariant == listVariant[index]
                    ? whiteColor
                    : greyColor,
                onTap: () {
                  setState(() {
                    selectVariant = listVariant[index];
                  });
                },
              )),
    );
  }

  Widget descriptionsProduct() {
    return const TextHelp(
        fonSize: 14,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w400,
        heightText: 1.5,
        color: textColor,
        text:
            "The Max Air 270 unit delivers unrivaled, all-day comfort. The sleek, running-inspired design roots you to everything Nike........");
  }

  Widget attributeList() {
    return SizedBox(
      height: 90,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: BoxShadowContainer(
                  onTap: () {
                    setState(() {
                      selected = index;
                      _controller.animateToPage(index,
                          duration: const Duration(milliseconds: 10),
                          curve: Curves.ease);
                      color = listShoes[index]["color"] ?? "";
                    });
                    print(color);
                  },
                  isBorder: selected == index ? true : false,
                  imageUrl: listShoes[index]["image"] ?? ""),
            );
          }),
    );
  }

  Widget showPictureProduct(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Stack(
          children: [
            Container(
              width: double.infinity,
              height: 75,
              decoration: const ShapeDecoration(
                shape: OvalBorder(
                    side: BorderSide(width: 2, color: backgroundColor)),
              ),
            ),
            Positioned(
              bottom: 20,
              left: 0,
              right: 0,
              child: Container(
                width: double.infinity,
                height: 68,
                decoration: const ShapeDecoration(
                  color: backgroundColor,
                  shape: OvalBorder(
                    side: BorderSide(width: 3, color: blueColor),
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 5,
              left: MediaQuery.of(context).size.width * 0.38,
              right: MediaQuery.of(context).size.width * 0.38,
              child: Container(
                alignment: Alignment.center,
                height: 36,
                decoration: BoxDecoration(
                    color: textColor, borderRadius: BorderRadius.circular(16)),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                          padding: EdgeInsets.zero,
                          constraints: const BoxConstraints(),
                          icon: const Icon(Icons.arrow_back_ios),
                          onPressed: () {
                            _controller.previousPage(
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.ease,
                            );
                          },
                          color: whiteColor),
                      IconButton(
                          padding: EdgeInsets.zero,
                          constraints: const BoxConstraints(),
                          icon: const Icon(Icons.arrow_forward_ios_outlined),
                          onPressed: () {
                            _controller.nextPage(
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.ease,
                            );
                          },
                          color: whiteColor),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        Positioned(
          left: 60,
          right: 60,
          bottom: 55,
          child: SizedBox(
            height: 150,
            width: 100,
            child: Padding(
              padding: const EdgeInsets.all(4),
              child: PageView.builder(
                reverse: true,
                controller: _controller,
                scrollDirection: Axis.horizontal,
                itemCount: listShoes.length,
                onPageChanged: (value) {
                  setState(() {
                    selected = value;
                  });
                },
                itemBuilder: (BuildContext context, int itemIndex) {
                  return CachesImage(
                    imageUrl: listShoes[itemIndex]["image"] ?? "",
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget titleProduct() {
    return const SizedBox(
      width: 300,
      child: TextHelp(
        text: "Cappuccino",
        fontWeight: FontWeight.w700,
        fonSize: 26,
        color: greyColor,
      ),
    );
  }
}
