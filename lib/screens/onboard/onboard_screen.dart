import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/screens/main_screen.dart';
import 'package:shose_shop/widget/main_btn.dart';

class OnBoardScreen extends StatefulWidget {
  const OnBoardScreen({super.key});

  @override
  State<OnBoardScreen> createState() => _OnBoardScreenState();
}

class _OnBoardScreenState extends State<OnBoardScreen> {
  List<Map<String, String>> Data = [
    {
      "text": "WELLCOME TO\n NIKE",
      "Image": shoseStyle,
      'text2': "",
      'text3': "Get Start",
    },
    {
      "text": "Let's Start Journey With Nike",
      "Image": shose2,
      'text2': "smart, Gorgeous & Fashionable \n Collection Explore Now",
      'text3': "Continue",
    },
    {
      "text": "You Have The \n Power To",
      "Image": shose3,
      'text2': "There are many beautiful And Attractive \n Plants To Your Room",
      'text3': "Continue",
    },
  ];
  int currenIndex = 0;
  PageController controller =
      PageController(viewportFraction: 1, keepPage: true);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: blueColor,
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            color: blueColor,
            height: MediaQuery.of(context).size.height,
          ),
          Positioned(
            bottom: 250,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                logo_nike,
                fit: BoxFit.cover,
                width: MediaQuery.of(context).size.width,
              ),
            ),
          ),
          Positioned(
            child: PageView.builder(
              physics: const ClampingScrollPhysics(),
              onPageChanged: (value) {
                setState(() {
                  currenIndex = value;
                });
              },
              controller: controller,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return shapeBoard(context,
                    boldText: Data[index]["text"],
                    image: Data[index]['Image'],
                    normal: Data[index]['text2'],
                    lableBtn: Data[index]["text3"], onTap: () {
                  setState(() {
                    currenIndex = index;
                    controller.nextPage(
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.ease,
                    );
                  });
                });
              },
              itemCount: Data.length,
            ),
          ),
          Positioned(
            bottom: 150,
            left: 0,
            right: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(
                Data.length,
                (index) => containerShape(
                  width: currenIndex == index ? 30 : 20,
                  color: currenIndex == index ? whiteColor : yellowColor,
                ),
              ),
            ),
          ),
          Positioned(
              bottom: 20,
              left: 0,
              right: 0,
              child: MainButton(
                  lable: currenIndex > 0 ? "Continue" : "Get Start",
                  onTap: () {
                    setState(() {
                      controller.nextPage(
                        duration: const Duration(milliseconds: 500),
                        curve: Curves.ease,
                      );
                      if (currenIndex == Data.length - 1) {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const MainScreen()));
                      }
                    });
                  }))
        ],
      ),
    );
  }
}

Widget shapeBoard(BuildContext context,
    {String? boldText,
    String? normal,
    String? image,
    String? lableBtn,
    required VoidCallback onTap,
    bool isTextBelow = false}) {
  return Stack(
    clipBehavior: Clip.none,
    children: [
      Positioned(
        left: 0,
        right: 0,
        child: Padding(
          padding: const EdgeInsets.only(top: 100),
          child: Text(
            boldText ?? "",
            textAlign: TextAlign.center,
            style: const TextStyle(
                fontFamily: "Raleway",
                fontSize: 30,
                fontWeight: FontWeight.w900,
                color: whiteColor),
          ),
        ),
      ),
      SizedBox(
        height: MediaQuery.of(context).size.height * 0.35,
      ),
      Positioned(
        bottom: 0,
        top: -70,
        left: 0,
        right: -20,
        child: Image.asset(
          image ?? "",
          width: 560,
          height: 420,
        ),
      ),
      Positioned(
        bottom: 250,
        left: 0,
        right: 0,
        child: Center(
            child: Text(
          normal ?? "",
          textAlign: TextAlign.center,
          style: const TextStyle(
              height: 1.5,
              fontFamily: "Raleway",
              color: whiteColor,
              fontSize: 14,
              fontWeight: FontWeight.w600),
        )),
      ),
    ],
  );
}

Widget containerShape({double? width, Color color = whiteColor}) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      width: width,
      height: 5,
      decoration:
          BoxDecoration(color: color, borderRadius: BorderRadius.circular(8)),
    ),
  );
}
