import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/screens/detail_check_out.dart';
import 'package:shose_shop/widget/app_bar.dart';
import 'package:shose_shop/widget/cart_list_product.dart';
import 'package:shose_shop/widget/dash_dash.dart';
import 'package:shose_shop/widget/header_content.dart';
import 'package:shose_shop/widget/main_btn.dart';
import 'package:shose_shop/widget/text_help.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({super.key});
  static const String routeName = "/cart_screen";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppbarCus(title: "Cart"),
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              const SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: TextHelp(
                    text: "3 Items",
                    fonSize: 16,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    return Slidable(
                        key: const ValueKey(0),
                        startActionPane: ActionPane(
                          extentRatio: 0.2,
                          motion: const ScrollMotion(),
                          //dismissible: DismissiblePane(onDismissed: () {}),
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                padding: const EdgeInsets.all(4),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius: BorderRadius.circular(8)),
                                width: 60,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    IconButton(
                                        padding: EdgeInsets.zero,
                                        constraints: const BoxConstraints(),
                                        icon: const Icon(Icons.add),
                                        onPressed: () {},
                                        color: whiteColor),
                                    const TextHelp(
                                      text: "2",
                                      fonSize: 14,
                                      fontWeight: FontWeight.w500,
                                      color: whiteColor,
                                      fontFamily: "Poppins",
                                    ),
                                    IconButton(
                                        padding: EdgeInsets.zero,
                                        constraints: const BoxConstraints(),
                                        icon: const Icon(Icons.minimize),
                                        onPressed: () {},
                                        color: whiteColor),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        endActionPane: ActionPane(
                          extentRatio: 0.2,
                          motion: const ScrollMotion(),
                          //dismissible: DismissiblePane(onDismissed: () {}),
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                padding: const EdgeInsets.all(4),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: redColor,
                                    borderRadius: BorderRadius.circular(8)),
                                width: 60,
                                child:
                                    const Icon(Icons.delete, color: whiteColor),
                              ),
                            )
                          ],
                        ),
                        child: const CardListProduct());
                  },
                  childCount: 20,
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              decoration: const BoxDecoration(color: Colors.white),
              // height: 250,
              padding: const EdgeInsets.all(16),
              child: Column(
                children: [
                  const HeaderContent(
                    color: greyColor,
                    leftText: "Subtotal",
                    rightText: "\$458.98",
                    fontSize: 16,
                  ),
                  const SizedBox(height: 8),
                  const HeaderContent(
                    color: greyColor,
                    fontSize: 16,
                    leftText: "Delivery",
                    rightText: "\$458.98",
                  ),
                  const SizedBox(height: 16),
                  const MySeparator(
                    color: greyLowColor,
                    height: 2,
                  ),
                  const SizedBox(height: 16),
                  const HeaderContent(
                    leftTextColor: greyColor,
                    fontSize: 16,
                    leftText: "Total Cost",
                    rightText: "\$458.98",
                  ),
                  const SizedBox(height: 32),
                  MainButton(
                    lable: "Continue",
                    onTap: () {
                      Navigator.pushNamed(context, DetailCheckOut.routeName);
                    },
                    color: mainColor,
                    colorText: whiteColor,
                    paddingBtn: 0,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
