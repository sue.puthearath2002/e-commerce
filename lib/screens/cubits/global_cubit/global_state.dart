part of 'global_cubit.dart';

class GlobalState {
  final bool? isLoading;
  final  int countCart;

  GlobalState({
    this.isLoading = false,
    this.countCart = 0,
  });

  GlobalState copyWith({
    bool? isLoading,
    int ? countCart,
  }) {
    return GlobalState(
      isLoading: isLoading ?? this.isLoading,
      countCart: countCart?? this.countCart,
    );
  }
}
