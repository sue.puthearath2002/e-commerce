import 'package:flutter_bloc/flutter_bloc.dart';
part 'global_state.dart';

class GlobalCubit extends Cubit<GlobalState> {
  GlobalCubit() : super(GlobalState());

  inCreaseQty({required  countQty}) {
    emit(state.copyWith(
      countCart: countQty++,
      
    ));
    print(state.countCart);
  }

  deCreaseQty({required  countQty}) {
    emit(state.copyWith(
      countCart: countQty--,
      
    ));
    print(state.countCart);
  }
}
