import 'package:flutter_bloc/flutter_bloc.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeState(isLoading: true));

  selectCategory({String? categorySelect, int? indexSelect}) {
    emit(state.copyWith(
      categoryName: categorySelect,
      selectIndex: indexSelect,
    ));
    print(state.categoryName);
  }

   onInitCart() async {
    emit(state.copyWith(
      cartNumber: state.cartCount?.length,
    ));
    print("oooo${state.cartNumber}");
  }

  inCreaseQty({required String nameProduct}) {
    final List<String> storeProducts = [];
    storeProducts.add(nameProduct);

    emit(state.copyWith(cartCount: storeProducts));
    onInitCart();
    print(state.cartCount);
  }

  deCreaseQty({required countQty}) {
    emit(state.copyWith(
      countCart: countQty--,
    ));
  }
  // selectCategoryByIndex({ int?indexSelect}) {
  //   emit(state.copyWith(
  //     selectIndex: indexSelect,
  //   ));
  //   print(state.selectIndex);
  // }
}
