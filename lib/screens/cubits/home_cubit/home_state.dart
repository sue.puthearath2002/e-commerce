part of 'home_cubit.dart';

class HomeState {
  final bool isLoading;
  String? categoryName;
  String? productName;
  int? cartNumber;
  int? selectIndex;
  final int countCart;
  final List<String>? cartCount;

  HomeState({
    this.categoryName = "",
    this.isLoading = false,
    this.selectIndex = 0,
    this.countCart = 0,
    this.cartCount,
    this.productName = "",
    this.cartNumber = 0,
  });

  HomeState copyWith({
    String? categoryName,
    bool? isLoading,
    int? selectIndex,
    int? cartNumber,
    int? countCart,
    String? productName,
    List<String>? cartCount,
  }) {
    return HomeState(
        categoryName: categoryName ?? this.categoryName,
        isLoading: isLoading ?? this.isLoading,
        selectIndex: selectIndex ?? this.selectIndex,
        countCart: countCart ?? this.countCart,
        cartCount: cartCount ?? this.cartCount,
        cartNumber: cartNumber??this.cartNumber,
        productName: productName ?? this.productName);
  }
}
