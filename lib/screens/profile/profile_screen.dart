import 'package:flutter/material.dart';
import 'package:shose_shop/widget/app_bar.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: AppbarCus(
        isDrawAble: true,
        title: "Profile",
      ),
    );
  }
}
