import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/screens/register_login/register_screen.dart';
import 'package:shose_shop/widget/main_btn.dart';
import 'package:shose_shop/widget/sliver_appbar.dart';
import 'package:shose_shop/widget/text_form_cus.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});
  static const String routeName = "/login";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      persistentFooterAlignment: AlignmentDirectional.bottomCenter,
      persistentFooterButtons: [
        Text.rich(
          TextSpan(
            children: [
              const TextSpan(
                text: 'Already have account ? ',
                style: TextStyle(
                  color: Color(0xFF6A6A6A),
                  fontSize: 16,
                  fontFamily: 'Raleway',
                  fontWeight: FontWeight.w500,
                  height: 0,
                ),
              ),
              WidgetSpan(
                child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, RegisterScreen.routeName);
                    },
                    child: const Text('Login')),
                style: const TextStyle(
                  color: Color(0xFF1A1D1E),
                  fontSize: 16,
                  fontFamily: 'Raleway',
                  fontWeight: FontWeight.w500,
                  height: 0,
                ),
              ),
            ],
          ),
          textAlign: TextAlign.center,
        )
      ],
      body: CustomScrollView(
        slivers: [
          const SliverAppbarCus(title: "Register Account"),
          const SliverToBoxAdapter(
            child: Text(
              "Fill Your Details Or Continue with \n Social Media",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xFF707B81),
                fontSize: 16,
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          const SliverToBoxAdapter(
            child: SizedBox(
              height: 24,
            ),
          ),
          const SliverToBoxAdapter(
            child: TextFormCus(
              lableText: "Your Name",
            ),
          ),
          const SliverToBoxAdapter(
            child: SizedBox(
              height: 16,
            ),
          ),
          const SliverToBoxAdapter(
            child: TextFormCus(
              lableText: "Email Address",
            ),
          ),
          const SliverToBoxAdapter(
            child: SizedBox(
              height: 16,
            ),
          ),
          const SliverToBoxAdapter(
            child: TextFormCus(
              lableText: "Password",
            ),
          ),
          const SliverToBoxAdapter(
            child: SizedBox(
              height: 8,
            ),
          ),
          const SliverToBoxAdapter(
              child: Padding(
            padding: EdgeInsets.only(right: 16),
            child: Text(
              'Recovery Password',
              textAlign: TextAlign.right,
              style: TextStyle(
                color: Color(0xFF707B81),
                fontSize: 12,
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w400,
                height: 0.11,
              ),
            ),
          )),
          const SliverToBoxAdapter(
            child: SizedBox(
              height: 16,
            ),
          ),
          SliverToBoxAdapter(
              child: MainButton(
            lable: "Sign In",
            onTap: () {},
            color: mainColor,
            colorText: whiteColor,
          )),
          SliverToBoxAdapter(
            child: MainButton(
              color: greyLowColor,
              lable: "Sign In with Google",
              onTap: () {},
              isIconsImage: true,
              iconSvg: googleSvg,
            ),
          ),
        ],
      ),
    );
  }
}
