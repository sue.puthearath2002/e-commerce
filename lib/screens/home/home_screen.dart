// ignore_for_file: must_be_immutable

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/screens/cubits/home_cubit/home_cubit.dart';
import 'package:shose_shop/screens/detail_product.dart';
import 'package:shose_shop/screens/register_login/login_screen.dart';
import 'package:shose_shop/widget/add_to_cart_btn.dart';
import 'package:shose_shop/widget/app_bar.dart';
import 'package:shose_shop/widget/boxWidget.dart';
import 'package:shose_shop/widget/cache_image.dart';
import 'package:shose_shop/widget/card_product.dart';
import 'package:shose_shop/widget/circle_btn.dart';
import 'package:shose_shop/widget/header_content.dart';
import 'package:shose_shop/widget/main_btn.dart';
import 'package:shose_shop/widget/new_arrival.dart';
import 'package:shose_shop/widget/rectangle_btn.dart';
import 'package:shose_shop/widget/search_container.dart';
import 'package:shose_shop/widget/text_help.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});

  final screenCubit = HomeCubit();

  List<String> listMenu = [
    "Oleato",
    "Hot Coffees",
    "Hot Teas",
    "Hot Drink",
    "Frappuccino",
    "Cold Coffees",
    "Iced Teas",
    "Cold Drinks",
  ];

  bool isClickAdd = false;

  List<String> listVariant = [
    "10%",
    "20%",
    "30%",
    "40%",
    "50%",
    "60%",
    "70%",
    "80%",
    "90%",
    "100%",
  ];

  String selectVariant = "";

  int a = 0;

  int b = 0;

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppbarCus(
        isDrawAble: true,
        title: "Explore",
      ),
      body: BlocProvider(
        create: (context) => HomeCubit(),
        child: BlocConsumer<HomeCubit, HomeState>(
          listener: (context, state) {},
          builder: (context, state) {
            return buildBody(context, state);
          },
        ),
      ),
    );
  }

  Widget buildBody(BuildContext context, HomeState state) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const SearchContainer(),
                CirCleBtn(
                  onTap: () {
                    Navigator.pushNamed(context, LoginScreen.routeName);
                  },
                  iconSvg: filterSvg,
                  width: 46,
                  height: 46,
                )
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            sliderShopNow(),
            const SizedBox(
              height: 16,
            ),
            const Align(
              alignment: Alignment.topLeft,
              child: Text(
                'Categories',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontStyle: FontStyle.normal,
                  fontFamily: 'Raleway',
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: greyColor,
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            listCategory(state),
            const SizedBox(
              height: 8,
            ),
            const HeaderContent(
              leftText: "Popular shoes",
              rightText: "See all",
            ),
            const SizedBox(
              height: 8,
            ),
            popularItems(),
            const SizedBox(
              height: 8,
            ),
            const HeaderContent(
              leftText: "New Arrival",
              rightText: "See all",
            ),
            const SizedBox(height: 16),
            const CardArrival(),
            const SizedBox(
              height: 8,
            ),
            const HeaderContent(
              leftText: "Recommend",
              rightText: "See all",
            ),
            const SizedBox(
              height: 8,
            ),
            recommendList(),
          ],
        ),
      ),
    );
  }

  Widget recommendList() {
    return SizedBox(
      height: 270,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: CardProduct(
                  onTap: () {
                    Navigator.pushNamed(context, DetailScreen.routeName);
                  },
                  image: "c",
                  status: "BEST SELLER",
                  nameProduct: "Bailey's Irish Cream and coffee",
                  priceProduct: "\$302.90"),
            );
          }),
    );
  }

  Widget popularItems() {
    return SizedBox(
      height: 270,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: CardProduct(
                onTap: () {
                  Navigator.pushNamed(context, DetailScreen.routeName);
                },
                image:
                    "https://upload.wikimedia.org/wikipedia/commons/3/3a/Double_flower_%28latte_art%29.jpg",
                status: "BEST SELLER",
                nameProduct: "Cappuccino",
                priceProduct: "\$4.09",
                onTapAdd: () {
                  context.read<HomeCubit>().inCreaseQty(nameProduct: "lllll");
                },
              ),
            );
          }),
    );
  }

  Future showListVariant(BuildContext context) {
    return showModalBottomSheet(
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: StatefulBuilder(builder: (context, setState) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextHelp(
                          text: "Sugar",
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w700,
                          fonSize: 16,
                          color: textColor,
                        ),
                      ),
                      Wrap(
                        children: List.generate(
                            listVariant.length,
                            (index) => MainButton(
                                  width: 60,
                                  color: selectVariant == listVariant[index]
                                      ? blueColor
                                      : skyColor,
                                  lable: listVariant[index],
                                  colorText: selectVariant == listVariant[index]
                                      ? whiteColor
                                      : greyColor,
                                  onTap: () {
                                    setState(() {
                                      selectVariant = listVariant[index];
                                    });
                                  },
                                )),
                      ),
                    ],
                  ),
                  StatefulBuilder(builder: (context, setState) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CirCleBtn(
                            onTap: () {},
                            width: 55,
                            height: 55,
                            paddingIconSvg: 10,
                            iconSvg: favoriteSvg,
                            colorIconSvg: greyColor,
                            colorContainer: greyLowColor,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: AddToCartBTN(
                              value: a <= 0 ? "0" : a.toString(),
                              onTapDecrease: () {
                                setState(() {
                                  a--;
                                });
                              },
                              onTapIncrease: () {
                                setState(() {
                                  a++;
                                });
                              },
                              onTap: () {
                                setState(() {
                                  isClickAdd = true;
                                });
                              },
                              isTap: isClickAdd,
                            ),
                          ),
                        ],
                      ),
                    );
                  })
                ],
              );
            }),
          );
        });
  }

  Widget listCategory(HomeState state) {
    return SizedBox(
      height: 60,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: listMenu.length,
          itemBuilder: (BuildContext context, int index) {
            return RectangleBtn(
              colorText: state.selectIndex == index ? whiteColor : greyColor,
              text: listMenu[index],
              colorBtn:
                  state.selectIndex == index ? mainColor : Colors.transparent,
              onTap: () {
                context.read<HomeCubit>().selectCategory(
                      categorySelect: listMenu[index],
                      indexSelect: index,
                    );
                print(state.categoryName);
              },
            );
          }),
    );
  }

  Widget sliderShopNow() {
    return CarouselSlider.builder(
      itemCount: 3,
      itemBuilder: (BuildContext context, int itemIndex, int pageViewIndex) {
        return const BoxWidget(
          padding: 0,
          height: 40,
          child: CachesImage(
            imageUrl:
                'https://d1csarkz8obe9u.cloudfront.net/posterpreviews/clothing-store-banner-design-template-e7332aaf6402c88cb4623bf8eb6f97e2_screen.jpg?ts=1620867237',
          ),
        );
      },
      options: CarouselOptions(
        autoPlay: true,
        enlargeCenterPage: true,
        viewportFraction: 0.8,
        aspectRatio: 2.0,
        initialPage: 2,
      ),
    );
  }
}
