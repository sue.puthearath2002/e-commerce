import 'package:flutter/material.dart';
import 'package:shose_shop/screens/carts/cart_screen.dart';
import 'package:shose_shop/screens/detail_check_out.dart';
import 'package:shose_shop/screens/detail_product.dart';
import 'package:shose_shop/screens/register_login/login_screen.dart';
import 'package:shose_shop/screens/register_login/register_screen.dart';
import 'package:shose_shop/splash_screen.dart';

import 'screens/main_screen.dart';

// 1,0 r -> l
// -1,0 l -> r
// 0,1 b -> t
// 0,-1 t->b

class AppNavigator {
  // static get end => Offset.zero;
  // static get begin => const Offset(0, -1);
  // static get curve => Curves.ease;

  static SlideTransition _st(animation, child) {
    final tween = Tween(
      begin: const Offset(1.0, 0.0),
      end: Offset.zero,
    );
    final curvedAnimation = CurvedAnimation(
      parent: animation,
      curve: Curves.ease,
    );
    return SlideTransition(
      position: tween.animate(curvedAnimation),
      child: child,
    );
  }

  static Route<dynamic>? appRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => const SplashScreen());

      case SplashScreen.routeName:
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) {
            return const SplashScreen();
          },
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return _st(animation, child);
          },
        );
      case CartScreen.routeName:
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) {
            return const CartScreen();
          },
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return _st(animation, child);
          },
        );
      case DetailScreen.routeName:
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) {
            return const DetailScreen();
          },
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return _st(animation, child);
          },
        );
      case DetailCheckOut.routeName:
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) {
            return DetailCheckOut();
          },
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return _st(animation, child);
          },
        );
      case LoginScreen.routeName:
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) {
            return const LoginScreen();
          },
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return _st(animation, child);
          },
        );
      case RegisterScreen.routeName:
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) {
            return const RegisterScreen();
          },
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return _st(animation, child);
          },
        );
      default:
        return MaterialPageRoute(builder: (_) => const MainScreen());
    }
  }
}


