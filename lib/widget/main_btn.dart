import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shose_shop/constants.dart';

class MainButton extends StatelessWidget {
  final String lable;
  final Color? color;
  final double? height;
  final double? width;
  final VoidCallback? onTap;
  final Color? colorText;
  final Color? colorIcon;
  final double? paddingBtn;
  final bool? isIconsImage;
  final String? iconSvg;
  const MainButton(
      {super.key,
      required this.lable,
      this.color = whiteColor,
      this.height = 50,
      required this.onTap,
      this.colorText = textColor,
      this.paddingBtn = 16,
      this.width,
      this.isIconsImage = false,
      this.iconSvg = cartSvg,
      this.colorIcon = whiteColor});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(paddingBtn ?? 16),
      child: InkWell(
        onTap: onTap,
        child: Container(
          alignment: Alignment.center,
          height: height,
          width: width,
          //padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: color, borderRadius: BorderRadius.circular(8)),
          child: isIconsImage == false
              ? Text(
                  lable,
                  style: TextStyle(
                    color: colorText,
                    fontFamily: 'Raleway',
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.center,
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      iconSvg ?? "",
                      width: 24,
                      height: 24,
                      colorFilter: isIconsImage == false
                          ? ColorFilter.mode(colorIcon!, BlendMode.srcIn)
                          : null,
                    ),
                    const SizedBox(width: 20),
                    Text(
                      lable,
                      style: TextStyle(
                        color: colorText,
                        fontFamily: 'Raleway',
                        fontWeight: FontWeight.w600,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}
