import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/widget/circle_btn.dart';

class SliverAppbarCus extends StatelessWidget {
  const SliverAppbarCus({super.key, required this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      expandedHeight: 120,
      backgroundColor: backgroundColor,
      elevation: 0,
      leading: Padding(
        padding: const EdgeInsets.only(left: 10),
        child: CirCleBtn(
          onTap: () {
            Navigator.pop(context);
          },
          iconSvg: backSvg,
          width: 46,
          height: 46,
          colorContainer: whiteColor,
          colorIconSvg: greyColor,
          paddingIconSvg: 10,
        ),
      ),
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Color(0xFF2B2B2B),
            fontSize: 20,
            fontFamily: 'Raleway',
            fontWeight: FontWeight.w700,
            height: 0,
          ),
        ),
      ),
    );
  }
}
