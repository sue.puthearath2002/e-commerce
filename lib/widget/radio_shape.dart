import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';

class RadioShape extends StatelessWidget {
  const RadioShape({super.key, this.isSelect = true});
  final bool? isSelect;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 26,
      height: 26,
      padding: const EdgeInsets.all(3),
      decoration: BoxDecoration(
        border: Border.all(color: greyColor, width: 2),
        shape: BoxShape.circle,
      ),
      child: isSelect == true
          ? Container(
              decoration: const BoxDecoration(
                color: greyColor,
                shape: BoxShape.circle,
              ),
            )
          : null,
    );
  }
}
