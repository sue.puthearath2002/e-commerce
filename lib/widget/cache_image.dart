import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';

class CachesImage extends StatelessWidget {
  const CachesImage(
      {super.key,
      required this.imageUrl,
      this.width = 170,
      this.height = 150,
      this.isBorder = false});
  final String imageUrl;
  final double? width;
  final double? height;
  final bool? isBorder;
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      width: width,
      height: height,
      fit: BoxFit.cover,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          border: isBorder == true ? Border.all(color: redColor) : null,
          borderRadius: BorderRadius.circular(16),
          image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
              colorFilter: const ColorFilter.mode(
                  Colors.transparent, BlendMode.colorBurn)),
        ),
      ),
      placeholder: (context, url) => const SizedBox(
          width: 10,
          height: 10,
          child: Center(child: CircularProgressIndicator())),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
