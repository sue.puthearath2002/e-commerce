import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';

class DividerLine extends StatelessWidget {
  const DividerLine({super.key, this.color = greyLowColor});
  final Color? color;
  @override
  Widget build(BuildContext context) {
    return Divider(
      color: color,
      height: 0.5,
    );
  }
}
