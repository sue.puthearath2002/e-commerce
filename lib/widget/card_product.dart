import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/widget/text_help.dart';

import 'cache_image.dart';

class CardProduct extends StatelessWidget {
  const CardProduct({
    super.key,
    this.image,
    this.status,
    this.nameProduct,
    this.priceProduct,
    this.IsFullFavorite = false,
    this.onTap,
    this.onTapAdd,
  });
  final String? image;
  final String? status;
  final String? nameProduct;
  final String? priceProduct;
  final bool? IsFullFavorite;
  final VoidCallback? onTap;
  final VoidCallback? onTapAdd;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Container(
            decoration: ShapeDecoration(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: CachesImage(
                      imageUrl: image ?? "",
                      //'https://www.golfdigest.com/content/dam/images/golfdigest/products/2022/3/3/20220302_NikeAirJordanLows1.jpg',
                    ),
                  ),
                  const SizedBox(height: 8),
                  TextHelp(
                    text: status ?? "",
                    fonSize: 12,
                    color: mainColor,
                  ),
                  const SizedBox(height: 4),
                  SizedBox(
                    width: 170,
                    child: TextHelp(
                      text: nameProduct ?? "",
                      fontWeight: FontWeight.w600,
                      color: textColor,
                    ),
                  ),
                  const SizedBox(height: 8),
                  TextHelp(
                    text: priceProduct ?? "",
                    fonSize: 14,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w500,
                    color: greyColor,
                  )
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: InkWell(
              onTap: onTapAdd,
              child: Container(
                decoration: const BoxDecoration(
                  color: mainColor,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(16),
                    topLeft: Radius.circular(16),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SvgPicture.asset(
                    addSvg,
                    colorFilter:
                        const ColorFilter.mode(whiteColor, BlendMode.srcIn),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              decoration: const BoxDecoration(
                //color: mainColor,
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(14),
                  topLeft: Radius.circular(14),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SvgPicture.asset(
                  IsFullFavorite == false ? favoriteSvg : favoriteFullSvg,
                  width: 30,
                  height: 30,
                  colorFilter: ColorFilter.mode(
                      IsFullFavorite == false ? greyColor : redColor,
                      BlendMode.srcIn),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
