import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/screens/carts/cart_screen.dart';

import 'circle_btn.dart';

class AppbarCus extends StatefulWidget implements PreferredSizeWidget {
  final bool? isDrawAble;
  final String title;
  final VoidCallback? showDrawer;

  const AppbarCus({
    super.key,
    this.isDrawAble = false,
    required this.title,
    this.showDrawer,
  });

  @override
  State<AppbarCus> createState() => _AppbarCusState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _AppbarCusState extends State<AppbarCus> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: widget.isDrawAble == false
          ? Padding(
              padding: const EdgeInsets.only(left: 10),
              child: CirCleBtn(
                onTap: () {
                  Navigator.pop(context);
                },
                iconSvg: backSvg,
                width: 46,
                height: 46,
                colorContainer: whiteColor,
                colorIconSvg: greyColor,
                paddingIconSvg: 10,
              ),
            )
          : GestureDetector(
              onTap: widget.showDrawer,
              child: Container(
                padding: const EdgeInsets.only(left: 10, right: 16),
                child: SvgPicture.asset(
                  drawAbleSvg,
                ),
              )),
      backgroundColor: backgroundColor,
      elevation: 0,
      centerTitle: true,
      title: Text(
        widget.title,
        style: const TextStyle(
            fontFamily: "Raleway",
            fontWeight: FontWeight.w600,
            color: greyColor),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: CirCleBtn(
            onTap: () {
              Navigator.pushNamed(context, CartScreen.routeName);
            },
            isRedNote: true,
            iconSvg: cartSvg,
            width: 46,
            height: 46,
            paddingIconSvg: 10,
            colorContainer: whiteColor,
            colorIconSvg: greyColor,
          ),
        ),
      ],
    );
  }
}
