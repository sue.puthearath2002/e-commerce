import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/widget/cache_image.dart';
import 'package:shose_shop/widget/text_help.dart';

class CardListProduct extends StatelessWidget {
  const CardListProduct({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 100,
        width: double.infinity,
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.circular(16),
          boxShadow: const [
            BoxShadow(
              color: shadowColor,
              blurRadius: 48,
              offset: Offset(0, 2),
              spreadRadius: 0,
            )
          ],
        ),
        child: const Row(
          children: [
            CachesImage(
              imageUrl:
                  "https://solestepping.co.uk/cdn/shop/products/Picture7_1d5353e4-87d8-4cec-8789-934d2e14f6ca_800x.jpg?v=1625406388",
              width: 130,
              height: 90,
            ),
            SizedBox(
              width: 20,
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextHelp(
                    text: "Nike Air max 200",
                    fontWeight: FontWeight.w600,
                    color: textColor,
                    fontFamily: "Raleway",
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextHelp(
                    text: "\$67.98",
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w500,
                    color: greyColor,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
