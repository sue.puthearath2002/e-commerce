import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';

class TextHelp extends StatelessWidget {
  const TextHelp(
      {super.key,
      required this.text,
      this.fonSize = 16,
      this.fontFamily = 'Raleway',
      this.color = greyColor,
      this.fontWeight = FontWeight.w500,
      this.heightText,
      this.isOverFlow = false});
  final String text;
  final FontWeight? fontWeight;
  final double? fonSize;
  final String? fontFamily;
  final Color? color;
  final double? heightText;
  final bool? isOverFlow;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width:
          isOverFlow == true ? MediaQuery.of(context).size.width * 0.6 : null,
      child: Text(
        text,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            height: heightText,
            fontStyle: FontStyle.normal,
            fontFamily: fontFamily,
            fontSize: fonSize,
            fontWeight: fontWeight,
            color: color),
      ),
    );
  }
}
