// ignore: file_names
import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';

class BoxWidget extends StatelessWidget {
  final Widget child;
  final double width ;
  final double height;
  final double padding;
  final double borderRadius;
  const BoxWidget({
    super.key,
    required this.child,
    this.height = 60,
    this.width = double.infinity,
    this.borderRadius = 16,
    this.padding =16,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:  EdgeInsets.all(padding),
      width: width,
      height: height,
      decoration: BoxDecoration(
          color: redColor, borderRadius: BorderRadius.circular(borderRadius)),
      child: child,
    );
  }
}
