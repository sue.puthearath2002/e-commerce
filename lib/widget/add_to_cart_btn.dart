import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/widget/main_btn.dart';

class AddToCartBTN extends StatefulWidget {
  const AddToCartBTN(
      {super.key,
      this.isTap = false,
      required this.onTap,
      required this.onTapIncrease,
      required this.onTapDecrease,
      required this.value});
  final bool isTap;
  final VoidCallback onTap;
  final VoidCallback onTapIncrease;
  final VoidCallback onTapDecrease;
  final String value;
  @override
  State<AddToCartBTN> createState() => _AddToCartBTNState();
}

class _AddToCartBTNState extends State<AddToCartBTN> {
  @override
  Widget build(BuildContext context) {
    return widget.isTap == false
        ? MainButton(
            iconSvg: cartSvg,
            isIconsImage: true,
            lable: "ADD TO CART",
            onTap: widget.onTap,
            colorText: whiteColor,
            color: mainColor,
          )
        : actionBTN();
  }

  Widget actionBTN() {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Container(
        height: 50,
        width: double.infinity,
        alignment: Alignment.center,
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
            color: mainColor, borderRadius: BorderRadius.circular(8)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              onPressed: widget.onTapDecrease,
              icon: SizedBox(
                width: 10,
                height: 20,
                child: SvgPicture.asset(
                  increaseSvg,
                  colorFilter:
                      const ColorFilter.mode(whiteColor, BlendMode.srcIn),
                ),
              ),
            ),
            Text(
              widget.value,
              style: const TextStyle(
                  color: whiteColor, fontWeight: FontWeight.bold, fontSize: 20),
            ),
            IconButton(
              onPressed: widget.onTapIncrease,
              icon: SizedBox(
                width: 30,
                height: 30,
                child: SvgPicture.asset(
                  addBoldSvg,
                  colorFilter:
                      const ColorFilter.mode(whiteColor, BlendMode.srcIn),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
