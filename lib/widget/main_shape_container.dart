import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';

class MainShapeContainer extends StatelessWidget {
  const MainShapeContainer(
      {super.key,
      this.colorContainer = whiteColor,
      this.child,
      this.isShadow = false,
      this.padding = 16});
  final Color? colorContainer;
  final Widget? child;
  final bool? isShadow;
  final double? padding;
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(padding!),
        decoration: BoxDecoration(
          color: colorContainer,
          borderRadius: BorderRadius.circular(16),
          boxShadow: isShadow == true
              ? [
                  BoxShadow(
                    color: shadowColor,
                    blurRadius: 48,
                    offset: Offset(0, 2),
                    spreadRadius: 0,
                  )
                ]
              : null,
        ),
        child: child!);
  }
}
