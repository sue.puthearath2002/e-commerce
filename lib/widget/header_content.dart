import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/widget/text_help.dart';

class HeaderContent extends StatelessWidget {
  const HeaderContent(
      {super.key,
      this.leftText,
      this.rightText,
      this.onTap,
      this.fontSize = 12,
      this.fontFamily,
      this.color = mainColor,
      this.leftTextColor});
  final String? leftText;
  final String? rightText;
  final VoidCallback? onTap;
  final double? fontSize;
  final String? fontFamily;
  final Color? color;
  final Color? leftTextColor;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        TextHelp(
          text: leftText ?? "",
          color: leftTextColor,
        ),
        InkWell(
          onTap: onTap,
          child: TextHelp(
            text: rightText ?? "",
            fontFamily: fontFamily,
            fonSize: fontSize,
            fontWeight: FontWeight.w600,
            color: color,
          ),
        ),
      ],
    );
  }
}
