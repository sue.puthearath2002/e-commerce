import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/widget/cache_image.dart';

class BoxShadowContainer extends StatelessWidget {
  const BoxShadowContainer(
      {super.key, required this.imageUrl, this.isBorder = false, this.onTap});
  final String imageUrl;
  final bool? isBorder;
  final VoidCallback? onTap;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: whiteColor,
          border:
              isBorder == true ? Border.all(color: redColor, width: 2) : null,
          borderRadius: BorderRadius.circular(16),
          boxShadow: const [
            BoxShadow(
              color: shadowColor,
              blurRadius: 48,
              offset: Offset(0, 2),
              spreadRadius: 0,
            )
          ],
        ),
        child: CachesImage(width: 90, height: 30, imageUrl: imageUrl),
      ),
    );
  }
}
