import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';

class TextFormCus extends StatelessWidget {
  const TextFormCus({super.key, required this.lableText});
  final String lableText;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            lableText,
            style: const TextStyle(
              color: greyColor,
              fontSize: 16,
              fontFamily: 'Raleway',
              fontWeight: FontWeight.w500,
              height: 0.08,
            ),
          ),
          const SizedBox(height: 16),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8), color: greyLowColor),
            child: TextFormField(
              //textAlign: TextAlign.center,
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(16),
                border: InputBorder.none,
              ),
            ),
          )
        ],
      ),
    );
  }
}
