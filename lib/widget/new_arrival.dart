import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';
import 'package:shose_shop/widget/text_help.dart';

class CardArrival extends StatelessWidget {
  const CardArrival({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ShapeDecoration(
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextHelp(
                  fonSize: 12,
                  text: "Summer Sale",
                  fontWeight: FontWeight.w500,
                  color: subTextColor,
                ),
                SizedBox(height: 20),
                TextHelp(
                  fonSize: 36,
                  text: "15% OFF",
                  fontWeight: FontWeight.w900,
                  color: highlightTextColor,
                ),
              ],
            ),
            Container(
              width: 170,
              height: 100,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/kapotino.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
