import 'package:flutter/material.dart';
import 'package:shose_shop/constants.dart';

class RectangleBtn extends StatelessWidget {
  const RectangleBtn({
    super.key,
    this.text,
    this.colorBtn = whiteColor,
    this.onTap,
    this.colorText,
  });
  final String? text;
  final Color? colorText;
  final Color? colorBtn;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
          decoration: BoxDecoration(border: Border.all(color: mainColor),borderRadius: BorderRadius.circular(8),color: colorBtn),
          // decoration: ShapeDecoration(
          //   color: colorBtn??greyColor,
          //   shape: RoundedRectangleBorder(
              
          //     borderRadius: BorderRadius.circular(8),
          //   ),
          // ),
          child: Text(
            text ?? "",
            style: TextStyle(
              color: colorText,
              fontFamily: "Poppins",
              fontWeight: FontWeight.w400,
              letterSpacing: 1,
            ),
          ),
        ),
      ),
    );
  }
}
